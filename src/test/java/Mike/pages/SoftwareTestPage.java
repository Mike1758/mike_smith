package Mike.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SoftwareTestPage extends PageBase {
	public SoftwareTestPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	} 

	String startUrl = "http://software-testing.ru/";
	By clickTraining = By.xpath("(//*[@class='tpparenttitle'])[4]");
	By clickShow3 = By.xpath("//*[@value='3']");
	By clickShow5 = By.xpath("//*[@value='5']");
	By clickShow10 = By.xpath("//*[@value='10']");
	By checkClickShow = By.xpath("//*[@class='sem_row']/..");
	
	public int checkShow(){
		int size = driver.findElements(checkClickShow).size();
		return size;
	}
	
	
	
	public void clicktoTraining() throws InterruptedException{
			openUrl(startUrl);
			clickByElement(clickTraining);
			String t = driver.findElement(clickShow3).getText();
			System.out.println(t);
			clickByElement(clickShow3);
			System.out.println("AAAAAA"+checkShow());
			Thread.sleep(3000);
	}
	
}
