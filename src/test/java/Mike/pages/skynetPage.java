package Mike.pages;
import org.openqa.selenium.Keys;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
public class skynetPage extends PageBase {
public skynetPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
String skynetSiteStart = "http://test.skynet.managementevents.com/";
String skynetSiteList = "http://test.skynet.managementevents.com/event/212/representatives";
String skynetSiteList1 = "http://test.skynet.managementevents.com/event/212/event-participant-roles";
String skynetSiteList2 = "http://test.skynet.managementevents.com/event/2/partners";
String nameLogin = "xsolve";
String passwordName = "xs0lv3";
String namePeople = "Scholz";
By loginField = By.name("username");
By passwordField = By.xpath("//*[@ngcontrol='password']");
By clickToSignIn = By.xpath("//*[@class='btn btn-primary']");
By clickToSelectAll = By.xpath("(//*[@type='checkbox'])[1]");
By checkCheckBox = By.xpath("(//*[@type='checkbox'])");
By thingWhichWait = By.xpath("//*[text()='Dashboard']");
By clickToClear = By.xpath("//*[@class='btn btn-default btn-primary clear-selection-btn-margin']");
By clickToSort = By.xpath("//*[@class='ng-grid-heading'][2]");
By sendTextName = By.xpath("(//*[@type='text'])[3]");
By clickToGoUser = By.xpath("//*[@href='/representatives/edit/158067']");
By waitVaryable = By.id("thisIsMainLoader");
By idField = By.xpath("//*[contains(text(),283662)]");
By lastNameField = By.xpath("//*[text()='Scholz']");
By firstNameField = By.xpath("//*[text()='Jessica']");
By companyField = By.xpath("//*[text()='BEITEN BURKHARDT Rechtsanwaltsgesellschaft mbH']");
By meAccountField = By.xpath("//*[text()='No']");
By checkSortFirstName = By.xpath("(//*[@class='ng-grid-cell'])[32]");


	public void singIn(){
		openUrl(skynetSiteStart);
		sendText(loginField, nameLogin);
		sendText(passwordField, passwordName);
		clickByElement(clickToSignIn);
		waitElement(thingWhichWait);
	}
	
	public void clickCheckBox() {
		openUrl(skynetSiteList);
		waitDisappearElement(waitVaryable);
		clickByElement(clickToSelectAll);
		waitDisappearElement(waitVaryable);
	}

	public  List<WebElement> checkCheckBox(){
		List<WebElement> elems = driver.findElements(checkCheckBox);
		return elems;
	}
	
	public int  numOfCheckBox(){
		int elems = driver.findElements(checkCheckBox).size();
		return elems;
	}
	
	public void clearAndSort() {
		clickByElement(clickToClear);
		waitDisappearElement(waitVaryable);
		clickByElement(clickToSort);
		waitDisappearElement(waitVaryable);
		clickByElement(clickToSort);
		waitDisappearElement(waitVaryable);
		
	}

	public void sortOnTextAndCheck() {	
		sendText(sendTextName, namePeople);
		waitDisappearElement(waitVaryable);
		sendKey(sendTextName, Keys.ENTER);
		waitDisappearElement(waitVaryable);
		clickByElement(clickToGoUser);
	}
	
	public String checkSort() {
		openUrl(skynetSiteList2);
		String text = driver.findElement(checkSortFirstName).getText();
		return text;
	}
	
	public String returnId(){
		String text = driver.findElement(idField).getText();
		return text;
	}
	
	public String returnLastName(){
		String text = driver.findElement(lastNameField).getText();
		return text;
	}

	public String returnFirstName(){
		String text = driver.findElement(firstNameField).getText();
		return text;
	}
	
	public String returnCompany(){
		String text = driver.findElement(companyField).getText();
		return text;
	}
	
	public String returnMyaccount(){
		String text = driver.findElement(meAccountField).getText();
		return text;
	}
}

