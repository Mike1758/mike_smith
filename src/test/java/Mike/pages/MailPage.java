package Mike.pages;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MailPage {	
	private WebDriver driver;
	String mailsite = "https://mail.ru/?from=logout&ref=main";
	String loginText = "steam.mihail170996";
	String passwordText = "893200Mih";
	String recipientId = "mihaill170996@yahoo.com";
	String themeMessage = "testmail";
	String messageText = "Lol";
	By loginField = By.name("Login");
	By passwordField = By.name("Password");
	By clickToIn = By.id("mailbox__auth__button");
	By writeLetter = By.xpath("//*[text()='�������� ������']");
	By recipient = By.xpath("//textarea[@class='js-input compose__labels__input'][1]");
	By recipient1 = By.xpath("//*[@class='js-input compose__labels__input'][2]");
	By theme = By.xpath("//*[@class='b-input']");
	By message = By.xpath("//*[@class='mceIframeContainer mceFirst mceLast']/iframe");
	By frame = By.id("tinymce");
	By send = By.xpath("(//*[@class='b-toolbar__btn__text'])[1]");
	By numberOfMessage = By.xpath("//*[@class='b-nav__item__count']");

		public MailPage(WebDriver driver) {
	        this.driver = driver;
	    }
		
		public void openUrl(){
			driver.get(mailsite);
		}
		 
		 public void clickByElement(By element){
			 waitElement(element);
			 driver.findElement(element).click();
		 }
		 
		 public void sendText(By element, String text){
			 waitElement(element);
			 driver.findElement(element).sendKeys(text);
		 }
		 
		 public String getTheText(){
			String text = driver.findElement(numberOfMessage).getText();
			return text;
		 }
		 
		 public void waitElement(By element){
			 (new WebDriverWait(driver, 10))
		        .until(ExpectedConditions.visibilityOfElementLocated(element));
		 }
		 
		 public void writeTextMessage(String text) {
			 driver.switchTo().frame(driver.findElement(message));
			 clickByElement(frame);
			 sendText(frame, text);
			driver.switchTo().defaultContent();
		 }
		
		 public void autification(){
			 openUrl();
			 sendText(loginField, loginText);
			 sendText(passwordField, passwordText);  
			 clickByElement(clickToIn);
		 }
		 
		 public void clickNewMessage(){
			clickByElement(writeLetter);
			waitElement(recipient1);
		 }
		 
		 public void sendMail(){
			 clickByElement(recipient1);
			 sendText(recipient,recipientId);
			 sendText(theme,themeMessage);
			 writeTextMessage(messageText);
			 clickByElement(send);
		 }
	}
		 
		 
		